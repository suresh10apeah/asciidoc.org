import hljs from 'highlight.js/lib/highlight.js'
import xmlLanguage from 'highlight.js/lib/languages/xml.js'
import asciidocLanguage from 'highlight.js/lib/languages/asciidoc.js'
import markdownLanguage from 'highlight.js/lib/languages/markdown.js'
import rubyLanguage from 'highlight.js/lib/languages/ruby.js'
import initHljsNumbers from 'highlightjs-line-numbers.js'

export function init () {
  hljs.registerLanguage('asciidoc', asciidocLanguage)
  hljs.registerLanguage('xml', xmlLanguage)
  hljs.registerLanguage('markdown', markdownLanguage)
  hljs.registerLanguage('ruby', rubyLanguage)
  initHljsNumbers(hljs)
  hljs.registerLanguage('rst', function (e) {
    return {}
  })

  if (!hljs.initHighlighting.called) {
    hljs.initHighlighting.called = true
    ;[].slice.call(document.querySelectorAll('pre.highlight > code')).forEach(function (el) {
      hljs.highlightBlock(el)
    })
    hljs.initLineNumbersOnLoad()
  }
}
