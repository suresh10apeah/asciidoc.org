---
layout: main
---
= AsciiDoc
:includedir@: ./examples
//:tagline: Publish presentation-rich content written using a concise and comprehensive authoring format.
:tagline: Publish presentation-rich content from a concise and comprehensive authoring format.
:description: AsciiDoc is a human-readable, text editor-friendly document format evolved from plain text markup conventions and semantically analogous to XML schemas like DocBook.
AsciiDoc is used to write notes, articles, documentation, books, web pages, slides, man pages, etc.
:url-asciidoc-docs: https://docs.asciidoctor.org/asciidoc/latest
:url-awesome-asciidoc: https://gitlab.eclipse.org/eclipse-wg/asciidoc-wg/asciidoc.org/-/blob/main/awesome-asciidoc.adoc
:url-asciidoc-wg: https://asciidoc-wg.eclipse.org
:url-asciidoc-chat: https://chat.asciidoc.org

AsciiDoc is a plain text markup language for writing technical content.
It's packed with semantic elements and equipped with features to modularize and reuse content.
AsciiDoc content can be composed using a text editor, managed in a version control system, and published to multiple output formats.

xref:#try[Get started,role=button]
{url-asciidoc-docs}/syntax-quick-reference/[Quick Reference^,role=button invert]

image::./assets/img/intellij-asciidoc-editor.png[IntelliJ AsciiDoc Plugin in action]

[#users.user]
== Trusted by developers and technical writers worldwide

[unstyled.companies]
* image:./assets/img/couchbase-logo-white.svg[]
* image:./assets/img/mulesoft-logo-white.svg[]
* image:./assets/img/vmware-logo-white.svg[role=vmware]
* image:./assets/img/neo4j-logo-white.svg[]
* image:./assets/img/redhat-logo-white.svg[]
* image:./assets/img/cloudbees-logo-white.svg[]
//* image:./assets/img/fauna-logo-white.svg[]
//* image:./assets/img/fedora-logo-white.svg[]

[#about]
== One language, multiple outputs: Publish READMEs, books, and everything in between

AsciiDoc provides all the semantic elements you need to write and publish technical books.
You'll also find AsciiDoc to be an ideal fit for documentation.
And yet, it's simple enough to use for READMEs or taking notes.
Explore the possibilities by browsing these screenshots.

[.tab]
--
[discrete]
=== Docs Site

image::./assets/img/docs-site-mulesoft.png[]
--

[.tab]
--
[discrete]
=== Article

image::./assets/img/blog-openliberty.png[]
--

[.tab]
--
[discrete]
=== Book

image::./assets/img/book-taming-thymeleaf.png[role=book]
--

[.tab]
--
[discrete]
=== Slides

image::./assets/img/slide-deck-java9-to-13.png[]
--

[.tab]
--
[discrete]
=== README

image::./assets/img/readme-jekyll-asciidoc.png[]
--

[.tab]
--
[discrete]
=== man page

image::./assets/img/man-page-git-for-each-ref.png[]
--

[#try-it-out.try]
== Try AsciiDoc now!: Experience the magic of a lightweight markup language

When you write in AsciiDoc, you use plain text.
That means the bulk of what you type are the words you want to communicate.
You only add markup characters when you need to encode meaning that can't otherwise be inferred.
For example, a section title starts with a series of equals signs and an unordered list item begins with one or more asterisks.
[.attn]#Try writing AsciiDoc in the editor below to see for yourself!#

[#editor-code,asciidoc]
------
= Hello, AsciiDoc!

This is an interactive editor.
Use it to try https://asciidoc.org[AsciiDoc].

== Section Title

* A list item
* Another list item

[,ruby]
----
puts 'Hello, World!'
----
------

[#compare.compare]
== How AsciiDoc stacks up: Compare AsciiDoc to other markup languages

AsciiDoc is designed to strike a balance between systematic, machine-oriented syntax and natural language.
This design affords AsciiDoc the ability to capture and encode nearly all the semantics of a highly-structured language while still being readable in source form.

Want to see how AsciiDoc stacks up against alternatives?
Browse the sample documents in this section to compare.

[.tab.compare]
--
[discrete]
=== DocBook

.index.xml
[.editor,xml]
----
include::{includedir}/db-sample.xml[]
----

DocBook is an **XML schema** for writing books and manuals about technical subjects.
It has an extensive catalog of tags for denoting content structures and elements.
Although **well-supported by tools**, writing in DocBook is tedious because the **content is overshadowed** by the markup, there are **a lot of tags** to remember, and **XML indentation** can be a major distraction.

.index.adoc
[.editor,asciidoc]
------
include::{includedir}/db-sample.adoc[]
------

.chapter-1.adoc
[.editor,asciidoc]
------
include::{includedir}/db-sample-chapter-1.adoc[]
------

.chapter-2.adoc
[.editor,asciidoc]
------
include::{includedir}/db-sample-chapter-2.adoc[]
------

**Content is the central focus** in AsciiDoc.
The document starts off with **no ceremonial prologue**, much of the structure is inferred from its **line-oriented** arrangement, there's **no indentation** needed, and content is marked up with **shorthand notations** instead of XML tags.
And yet, you can still produce DocBook from AsciiDoc to tie into existing toolchains.
--

[.tab.compare]
--
[discrete]
=== Markdown

.index.md
[.editor,markdown]
----
include::{includedir}/md-sample.md[]
----

Markdown is a **lightweight** markup language for producing HTML.
Markdown builds on basic **plain text conventions** for formatting content.
While approachable to a broad audience, it **stops short** of being a technical writing language.
The need for **syntax extensions** quickly enters the picture.
In reality, Markdown is the basis for a variety of markup languages that often **deviate widely**.

.index.adoc
[.editor,asciidoc]
------
include::{includedir}/md-sample.adoc[]
------

AsciiDoc appears **strikingly similar** to Markdown, making way for an easy transition.
Where AsciiDoc shines is in **its depth**.
AsciiDoc provides all the essential elements in **technical writing** out of the box.
**No variants** needed.
Its syntax can be elaborated without having to fundamentally change the language, assuring users that it's still **standard AsciiDoc**.
--

[.tab.compare]
--
[discrete]
=== DITA

.index.dita
[.editor,html]
----
include::{includedir}/dita-sample.dita[]
----

DITA is an **XML schema** for representing a broad range of information.
Its architecture encourages **content modularity** and reuse, **portable references**, and **controlled extension** of its vocabulary.
These goals are met using a **complex system** of XML tags, concepts, and toolchains.
DITA's complexity means it has a substantial learning curve and is **sparsely supported**.

.index.adoc
[.editor,asciidoc]
------
include::{includedir}/dita-sample.adoc[]
------

AsciiDoc fits the same architectural goals into an **XML-free** package and is **widely supported**.
Documents can be partitioned into **units of content** and reused with the include directive.
Document references are defined **source-to-source** and translated into links between published files.
Syntax extensions invite **new markup and integrations** while keeping the language consistent.
--

[.tab.compare]
--
[discrete]
=== restructuredText

.index.rst
[.editor,rst]
----
include::{includedir}/rst-sample.rst[]
----

reStructuredText is **plain text markup** for use in docstrings and formal documentation.
It offers an extensible syntax for producing structured output.
It's **line-oriented** design keeps the content clear and the separation of blocks evident.
However, its reliance on **indentation**, non-traditional notation, and **syntax variations** make the language **esoteric** and hard for newcomers to grasp.

.index.adoc
[.editor,asciidoc]
------
include::{includedir}/rst-sample.adoc[]
------

AsciiDoc aims to provide an **easy-to-read**, what-you-see-is-what-you-get syntax.
It attains this goal by making most **indentation insignificant**, using clear block boundaries, and relying on **conventional notation**.
AsciiDoc is composed of **a few patterns**, such as delimited blocks, macros, and formatting pairs.
A newcomer can pick up the syntax mostly by intuition alone.
--

[.tab.compare]
--
[discrete]
=== HTML

.index.html
[.editor,html]
----
include::{includedir}/html-sample.html[]
----

As the most basic **building block** of the web, HTML gives structure to **web content**.
But it also assumes a lot of **presentation responsibilities**.
It's rare to find an HTML document focused solely on content.
Its forte is in assembling **high fidelity web pages**, not as a technical writing format.
It has all the **ceremony of XML** with only a fraction of its presentation-agnostic semantics.

.index.adoc
[.editor,asciidoc]
------
include::{includedir}/html-sample.adoc[]
------

Although AsciiDoc was initially conceived as a DocBook shorthand, it's now mostly used to **generate HTML**.
By producing the HTML, content is **not impacted by redesigns** and **content variations**, like slides or eBooks, can be created from the same source.
AsciiDoc processors bundle a **stylesheet** that requires **no web development** to achieve a professional look.
--

[#specifications]
== Specification process: Governed by a language specification, always evolving

The AsciiDoc Language specification was established to ensure that AsciiDoc continues to evolve and that it's processed consistently by implementations across language runtimes, authoring environments, and application integrations.
The specification is managed and governed by the AsciiDoc Language project and, at a higher level, by the AsciiDoc Working Group at the Eclipse Foundation.
Development of the specification is currently underway.

[.actions]
{url-asciidoc-wg}[Get involved,role="action engage"]
{url-asciidoc-chat}[Join the conversation,role="action chat"]

[#tools]
== A growing ecosystem: AsciiDoc Tools & Support

[.card]
--
[discrete]
=== Author

Write, validate, and preview your AsciiDoc content in your favorite text editor, IDE, or browser.

{url-awesome-asciidoc}#author[Explore,role=action]
--

[.card]
--
[discrete]
=== Convert

Convert AsciiDoc documents to HTML, DocBook, PDF, and more using an AsciiDoc processor.

{url-awesome-asciidoc}#convert[Explore,role=action]
--

[.card]
--
[discrete]
=== Publish

Publish content using site generators or build tools that understand how to process AsciiDoc.

{url-awesome-asciidoc}#publish[Explore,role=action]
--

{url-awesome-asciidoc}[More Awesome AsciiDoc,role=explore action]

[#docs]
== Get started with AsciiDoc today

[.actions]
{url-asciidoc-docs}[View Documentation]
{url-asciidoc-chat}[Join the chat,role="action chat"]
